import React, { useState } from 'react';

function CatLogin()
{    
    const url = "https://catfact.ninja/fact";   
    
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');
    const [cat, setCat] = useState([]);

    const getCats = async () => {
        try {
          const response = await fetch(url);
          const cat = await response.json()
          setCat(cat.fact);
        } 
        catch (error) {
          console.log(error)
        }
    };

    const onChangeLogin = (e: React.ChangeEvent<HTMLInputElement>) => {
        const newValue = e.target.value;
        setLogin(newValue);
    }

    const onChangePassword = (e: React.ChangeEvent<HTMLInputElement>) => {
        const newValue = e.target.value;
        setPassword(newValue);
    }

    async function handleClick() {
        await getCats();
        if (login == 'User'
            && password == '1')
            alert(`Success! Get your cat fact:\n${cat}`);
        else
            alert(`Wrong login or password! Login: ${login}, Password: ${password}`);
      }
    
    return (        
        <form className='authorization'>            
            <div>
                <input onChange={onChangeLogin}
                    placeholder='login' 
                    type='login' />
            </div>
            <div>
                <input onChange={onChangePassword}
                    placeholder='password'  
                    type='password' />
                <br/>
            </div>            
            <button onClick={handleClick} type='button'>Login</button>
            <br/>            
        </form>        
    );
}

export default CatLogin;
